
// var disp = document.getElementById('disp');

window.requestAnimFrame = (function(){
	return  window.requestAnimationFrame       ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame    ||
			function( callback ){
				window.setTimeout(callback, 1000 / 60);
			};
})();
function Canvas($el, config) {
	var propDict,
		animating = false;

	if ($el === undefined) {
		$el = document.createElement('canvas');
		document.getElementsByTagName('body')[0].appendChild($el);
	}
	function init() {
		$el.layerNames = {};
		if ($el.tagName == 'CANVAS') {
			$el.layers = [$el];
			$el.contextLayers = [$el.getContext("2d")];
			if ($el.hasAttribute('id'))
				$el.layerNames[$el.getAttribute('id')] = 0;
		} else {
			var canvs = [],
				ctxs = [],
				canvases = $el.getElementsByTagName('canvas');
			if (canvases.length) {
				for (var c=0; c<canvases.length; c++) {
					var canv = canvases[c];
					canvs.push(canv);
					ctxs.push(canv.getContext("2d"));
					if (canv.hasAttribute('id'))
						$el.layerNames[canv.getAttribute('id')] = c;
				}
			} else {
				var canv = document.createElement('canvas');
				$el.appendChild(canv);
				canvs.push(canv);
				ctxs.push(canv.getContext("2d"));
			}
			$el.layers = canvs;
			$el.contextLayers = ctxs;
		}

		$el.setLayer(0);
		markClean();
		var defaults = {
			zoom:		1,
			center:		{x:0,y:0},
			midpoint:	{x:0,y:0},
			focus:		{x:0,y:0},
			inverseY:	false,
			inverseZ:	false,
			zoomMin:	1/34000,
			zoomMax:	34000,
			scrollSpeed:1/1000,
			scaled:		false,
			shapeOrigin:false,
			clearBeforeDraw:true,
			panning:	true,
			zooming:	true,
			zoomPanning:true,
			minShapeZoom:0
		};
		$el.sizeFunct = {width:false,height:false};
		$el.space = [];
		$el.states = {
			zoom:{
				state:false,
				wasRunning:false
			},
			pan:{
				state:false,
				wasRunning:false
			},
			resize:{
				state:false,
				wasRunning:false
			}
		};
		propDict = {
			"fill":				"fillStyle",
			"stroke":			"strokeStyle",
			"line":				"strokeStyle",

			"fill-style":		"fillStyle",
			"stroke-style":		"strokeStyle",
			"line-style":		"strokeStyle",

			"shadow-color":		"shadowColor",
			"shadow-blur":		"shadowBlur",
			"shadow-offset-x":	"shadowOffsetX",
			"shadow-offset-y":	"shadowOffsetY",

			"line-cap":			"lineCap",
			"line-join":		"lineJoin",
			"line-width":		"lineWidth",
			"stroke-cap":		"lineCap",
			"stroke-join":		"lineJoin",
			"stroke-width":		"lineWidth",
			"miter-limit":		"miterLimit",
			"line-miter":		"miterLimit",
			"stroke-miter":		"miterLimit",

			"font":				"font",
			"font-style":		"font",
			"text":				"font",
			"text-style":		"font",
			"text-align":		"textAlign",
			"font-align":		"textAlign",
			"text-baseline":	"textBaseline",
			"font-baseline":	"textBaseline",


			"global-alpha":		"globalAlpha",
			"global-composite-operation":	"globalCompositeOperation",
			"data":				"data",

			// Custom
			'center':			'center',
			'midpoint':			'midpoint',
			'focus':			'focus',
			'dirty':			'dirty',
			'zoom':				'zoom',
			'inverse-y':		'inverseY',
			'inverse-z':		'inverseZ',
			'invert-y':			'inverseY',
			'invert-z':			'inverseZ',
			'context':			'context',
			'draw':				'draw',
			'before-draw':		'beforeDraw',
			'pre-draw':			'beforeDraw',
			'after-draw':		'afterDraw',
			'post-draw':		'afterDraw',


			// Too many events?
			'before-pan-start':		'beforePanStart',
			'pre-pan-start':		'beforePanStart',
			'pan-start':			'panStart',
			'after-pan-start':		'afterPanStart',
			'post-pan-start':		'afterPanStart',

			'before-pan-step':		'beforePanStep',
			'pre-pan-step':			'beforePanStep',
			'pan-step':				'panStep',
			'after-pan-step':		'afterPanStep',
			'post-pan-step':		'afterPanStep',

			'before-pan-stop':		'beforePanStop',
			'pre-pan-stop':			'beforePanStop',
			'pan-stop':				'panStop',
			'after-pan-stop':		'afterPanStop',
			'post-pan-stop':		'afterPanStop',


			'before-zoom-start':	'beforeZoomStart',
			'pre-zoom-start':		'beforeZoomStart',
			'zoom-start':			'zoomStart',
			'after-zoom-start':		'afterZoomStart',
			'post-zoom-start':		'afterZoomStart',

			'before-zoom-step':		'beforeZoomStep',
			'pre-zoom-step':		'beforeZoomStep',
			'zoom-step':			'zoomStep',
			'after-zoom-step':		'afterZoomStep',
			'post-zoom-step':		'afterZoomStep',

			'before-zoom-stop':		'beforeZoomStop',
			'pre-zoom-stop':		'beforeZoomStop',
			'zoom-stop':			'zoomStop',
			'after-zoom-stop':		'afterZoomStop',
			'post-zoom-stop':		'afterZoomStop',


			'before-resize-start':	'beforeResizeStart',
			'pre-resize-start':		'beforeResizeStart',
			'resize-start':			'resizeStart',
			'after-resize-start':	'afterResizeStart',
			'post-resize-start':	'afterResizeStart',

			'before-resize-step':	'beforeResizeStep',
			'pre-resize-step':		'beforeResizeStep',
			'resize-step':			'resizeStep',
			'after-resize-step':	'afterResizeStep',
			'post-resize-step':		'afterResizeStep',

			'before-resize-stop':	'beforeResizeStop',
			'pre-resize-stop':		'beforeResizeStop',
			'resize-stop':			'resizeStop',
			'after-resize-stop':	'afterResizeStop',
			'post-resize-stop':		'afterResizeStop',



			'zoom-min':			'zoomMin',
			'zoom-max':			'zoomMax',
			'scroll-speed':		'scrollSpeed',
			'scaled':			'scaled',
			'shape-origin':		'shapeOrigin',
			'clear-before-draw':'clearBeforeDraw',
			'panning':			'panning',
			'zoom-panning':		'zoomPanning',
			'zooming':			'zooming',
			'min-shape-zoom':	'minShapeZoom',

		};
		// Inflate propdict, for two-words: add twoWords, twowords and two_words
		for (var p in propDict) {
			var prop = propDict[p],
				nList = p.split(/-/g);
			if (nList.length > 1) {
				propDict[nList.join('')] = prop;
				propDict[nList.join('_')] = prop;

				for (var n=1; n<nList.length; n++)
					nList[n] = nList[n][0].toUpperCase()+nList[n].slice(1);

				propDict[nList.join('')] = prop;
				propDict[nList.join('_')] = prop;
			}
		}


		window.onresize = resize;
		$el.config(defaults);
		if (config !== undefined)
			$el.config(config);
		if (undefined !== Hammer)
			hammerInterAction();
	}
	function markClean() {
		for (var c in this.contextLayers)
			this.contextLayers[c].dirty = false;
	}
	function reDraw (run) {
		if (run === undefined) run = true;
		if (run)
			animating = window.requestAnimFrame(reDraw);
			// $el.animate();
		if ($el.clearBeforeDraw)
			$el.clear();
		$el.drawSpace();
	}
	function resize (ev) {

		if ($el.states.resize.state)
			$el.resizeStepEvent(ev);
		else $el.resizeStartEvent(ev);

		if (! ev) $el.resizeStopEvent();

	}
	function setMidpoint () {
		$el.midpoint = {
			x:$el.width/2,
			y:$el.height/2,
		};
	}

	$el.animate = function () {
		reDraw(true);
	}
	$el.animating = function () {
		return animating;
	}
	$el.stop = function () {
		if (animating)
			window.cancelAnimationFrame(animating);
		animating = false;
	}
	$el.drawSpace = function () {
		// var offset = this.getOffset();
		for (var c in this.contextLayers) {
			var ctx = this.contextLayers[c];
			ctx.save();
			ctx.scale(this.zoom, this.zoom);
			// ctx.translate(offset.x, offset.y);
		}
		this.offset = this.getOffset();

		this.scaled = true;

		if (this.beforeDraw)
			this.beforeDraw();
		for (var s in this.space)
			this.object(this.space[s]);
		if (this.draw)
			this.draw();
		if (this.afterDraw)
			this.afterDraw();

		for (var c in this.contextLayers)
			this.contextLayers[c].restore();
		this.scaled = false;
	}
	$el.reset = function () {
		this.center = {x:0,y:0};
		this.focus = {x:0,y:0};
		this.zoom = 1;
	}

	$el.setCenter = function (position) {
		this.center = position;
		this.offset = this.getOffset();
	}
	$el.setLayer = function (index) {
		var i = parseInt(index);
		if (isNaN(i))
			i = this.layerNames[index];
		if (i == undefined)
			i = 0;
		if(i >= this.contextLayers.length)
			i = this.contextLayers.length-1;


		this.context = this.contextLayers[i];
		this.layer = this.layers[i];
	}
	$el.size = function (prop, val) {
		if (prop === undefined)
			return {width:this.width,height:this.height};
		if (typeof prop == 'object' && prop instanceof Object) {
			if ('width' in prop)
				this.Width(prop.width);
			if ('height' in prop)
				this.Height(prop.height);
			return this;
		} else if (typeof prop == 'string') {
			if (val === undefined) {
				if (prop == 'width')
					return this.Width();
				if (prop == 'height')
					return this.Height();
			} else {
				if (prop == 'width')
					return this.Width(val);
				if (prop == 'height')
					return this.Height(val);
			}
		}
	}
	$el.Width = function (val) {
		if (val === undefined)
			return this.width;
		if (typeof val == 'function') {
			this.sizeFunct.width = val;
			resize(false);
		} else {
			this.sizeFunct.width = false;
			this.width = val;
		}
		setMidpoint();
		markClean();
		return this;
	}
	$el.Height = function (val) {
		if (val === undefined)
			return this.height;
		if (typeof val == 'function') {
			this.sizeFunct.height = val;
			resize(false);
		} else {
			this.sizeFunct.height = false;
			this.height = val;
		}
		setMidpoint();
		markClean();
		return this;
	}

	$el.clear = function () {
		for (var c in this.contextLayers) {
			var ctx = this.contextLayers[c];
			if (ctx.dirty) {
				if (this.scaled) {
					ctx.clearRect(0, 0, this.width/this.zoom, this.height/this.zoom);
					ctx.dirty = false;
				} else {
					ctx.clearRect(0, 0, this.width, this.height);
					ctx.dirty = false;
				}
			}
		}
	}

	$el.config = function (prop, val) {
		if (typeof prop == 'object' && prop instanceof Object) {
			for (var p in prop)
				this.set(p, prop[p]);
			return this;
		} else if (typeof prop == 'string') {
			if (val === undefined)
				return this.get(prop);
			else return this.set(prop, val);
		}
	}
	$el.set = function (prop, val) {
		$el[propDict[prop]] = val;
		return this;
	}
	$el.get = function (prop) {
		return $el[propDict[prop]];
	}

	$el.configCtx = function (prop, val) {
		if (typeof prop == 'object' && prop instanceof Object) {
			for (var p in prop)
				this.setCtx(p, prop[p]);
			return this;
		} else if (typeof prop == 'string') {
			if (val === undefined)
				return this.getCtx(prop);
			else return this.setCtx(prop, val);
		}
	}
	$el.setCtx = function (prop, val) {
		$el.context[propDict[prop]] = val;
		return this;
	}
	$el.getCtx = function (prop) {
		return $el.context[propDict[prop]];
	}

	$el.getOffset = function () { //position) {
		/* takes spacial x,y: outputs canvas x,y */
		/*

		                       _X________________________________
		                      Y|                   __________   |
		                       |                   |      C*|   |
		                       |                   |   B*   |   |
		                       |                   |________|   |
		                       |                         Z++    |
		                       |               |                |
		                       |              -A-               |
		                       |               |                |
		                       |                                |
		        E_________________________________              |
		        |              |                 |              |
		        |              |_________________|______________|
		        |                                |           Z=0
		        |                                |
		        |                                |
		        |                |               |
		        |               -D-              |
		        |                |               |
		        |                                |
		        |                                |
		        |                                |
		        |________________________________|
		                                      Z=0


		X,Y - canvas origin

		Z - zoom - zoom level

		A - midpoint - center of the canvas element's size

		B - focus - point of first zoom (in)

		C - focus - point of second zoom (out/in)

		D - A + E - relative center of A, when zooming C after B, assuming Z = 0

		E - center - equivalent translation of frame after zoom C

		*/
		return {
			x:
			 ((this.midpoint.x+this.focus.x)/this.zoom)-
				this.focus.x+this.center.x,
			y:
			 ((this.midpoint.y+this.focus.y)/this.zoom)-
				this.focus.y+this.center.y
		};

	}
	$el.position = function (position) {

		var i = 1;
		if (this.inverseY) i = -1;

		var z = 1;
		if (! this.scaled)
			z = this.zoom;
		return {
			x:(this.offset.x+position.x) * z,
			y:(this.offset.y+position.y*i) * z,
		};

	}
	$el.unPosition = function (position) {
		/* takes canvas x,y: outputs spacial x,y */

		var i = 1;
		if (this.inverseY) i = -1;
		return {
			x:((position.x-this.midpoint.x-this.focus.x)/this.zoom)+this.focus.x-this.center.x,
			y:(((position.y-this.midpoint.y-this.focus.y)/this.zoom)+this.focus.y-this.center.y)*i
		};
	}


	// Only three events...
	/*
		# START
		# beforeStart
		state
		# Start
		(events)
		# (Step)
		animation
		prevent
		# afterStart
		# (Stop)

		# STEP
		# beforeStep
		logic
		# Step
		(events)
		prevent
		# afterStep

		# STOP
		# beforeStop
		state
		# Stop
		(events)
		animation
		prevent
		# afterStop
	*/
	$el.panEvent = function (x, y) {
		this.center.x += x/this.zoom;
		this.center.y += y/this.zoom;
	}
	$el.zoomEvent = function (x, y, z) {

		var zsize = (1/this.zoom)-1;

		if (this.panning && this.zoomPanning) {
			this.center = {
				x:((this.midpoint.x+this.focus.x-x)*zsize)+this.center.x,
				y:((this.midpoint.y+this.focus.y-y)*zsize)+this.center.y
			};
			this.focus = {
				x:x-this.midpoint.x,
				y:y-this.midpoint.y
			};
		} else {
			this.focus = {
				x:0,
				y:0
			};
		}

		if (z >= 0.9) z = 0.9;
		if (z < -0.9) z = -0.9;
		this.zoom *= 1-z;
		if (this.zoom > this.zoomMax)
			this.zoom = this.zoomMax;
		if (this.zoom < this.zoomMin)
			this.zoom = this.zoomMin;
	}
	$el.resizeEvent = function () {
		var w = false,
			h = false;
		if (this.sizeFunct.width)
			w = this.sizeFunct.width();
		if (this.sizeFunct.height)
			h = this.sizeFunct.height();
		if (h) {
			this.height = h;
			for (var l in this.layers)
				this.layers[l].height = h;
		}
		if (w) {
			this.width = w;
			for (var l in this.layers)
				this.layers[l].width = w;
		}

		setMidpoint();
		markClean();
	}

	$el.panStepEvent = function (ev) {
		if (this.beforePanStep) {
			if (this.beforePanStep(ev) === false)
				return false;
		}

		this.panEvent(ev.movementX, ev.movementY);

		if (this.panStep)
			this.panStep();

		ev.preventDefault();

		if (this.afterPanStep)
			this.afterPanStep();
		return false;
	}
	$el.panStopEvent = function (ev) {
		if (this.beforePanStop)
			this.beforePanStop();

		this.states.pan.state = false;

		if (this.panStop)
			this.panStop();

		this.onmouseup = undefined;
		this.onmouseout = undefined;
		this.onmousemove = undefined;
		delete this.onmouseup;
		delete this.onmouseout;
		delete this.onmousemove;

		if (! this.states.pan.wasRunning)
			this.stop();

		ev.preventDefault();

		if (this.afterPanStop)
			this.afterPanStop();

		return false;
	}
	$el.panStartEvent = function (ev) {
		if (this.beforePanStart) {
			if (this.beforePanStart(ev) === false)
				return false;
		}

		this.states.pan.state = true;

		if (this.panStart)
			this.panStart();

		this.onmousemove = this.panStepEvent;
		this.onmouseup = this.panStopEvent;
		this.onmouseout = this.panStopEvent;

		this.panStepEvent(ev);

		this.states.pan.wasRunning = animating;
		if (! animating) this.animate();

		ev.preventDefault();

		if (this.afterPanStart)
			this.afterPanStart();
	}

	$el.zoomStopEvent = function () {
		if (this.beforeZoomStop)
			this.beforeZoomStop();

		this.states.zoom.state = false;

		if (this.zoomStop)
			this.zoomStop();

		if (! this.states.zoom.wasRunning)
			this.stop();

		if (this.afterZoomStop)
			this.afterZoomStop();
	}
	$el.zoomStepEvent = function (ev) {
		if (this.beforeZoomStep) {
			if (this.beforeZoomStep(ev) === false)
				return false;
		}
		var i = 1;
		if (this.inverseZ) i = -1;
		this.zoomEvent(ev.offsetX, ev.offsetY, ev.deltaY*this.scrollSpeed*i);

		if (this.zoomStep)
			this.zoomStep();

		window.clearTimeout(this.states.zoom.timeout);
		this.states.zoom.timeout = window.setTimeout(function() {
			$el.zoomStopEvent();
		}, 250);

		ev.preventDefault();

		if (this.afterZoomStep)
			this.afterZoomStep();
	}
	$el.zoomStartEvent = function (ev) {

		if (this.beforeZoomStart) {
			if (this.beforeZoomStart(ev) === false)
				return false;
		}

		this.states.zoom.state = true;

		if (this.zoomStart)
			this.zoomStart();

		this.zoomStepEvent(ev);

		this.states.zoom.wasRunning = animating;
		if (! animating) this.animate();

		ev.preventDefault();

		if (this.afterZoomStart)
			this.afterZoomStart();

	}

	$el.resizeStopEvent = function() {
		if (this.beforeResizeStop)
			this.beforeResizeStop();

		this.states.resize.state = false;

		if (this.resizeStop)
			this.resizeStop();

		if (! this.states.resize.wasRunning)
			this.stop();

		if (this.afterResizeStop)
			this.afterResizeStop();
	}
	$el.resizeStepEvent = function (ev) {
		if (this.beforeResizeStep) {
			if (this.beforeResizeStep(ev) === false)
				return false;
		}
		this.resizeEvent();

		if (this.resizeStep)
			this.resizeStep();

		window.clearTimeout(this.states.resize.timeout);
		if (ev) this.states.resize.timeout = window.setTimeout(function () {$el.resizeStopEvent();}, 250);

		if (this.afterResizeStep)
			this.afterResizeStep();
	}
	$el.resizeStartEvent = function (ev) {
		if (this.beforeResizeStart) {
			if (this.beforeResizeStart(ev) === false)
				return false;
		}

		this.states.resize.state = true;

		if (this.resizeStart)
			this.resizeStart();

		this.resizeStepEvent(ev);

		this.states.resize.wasRunning = animating;
		if (! animating) this.animate();

		if (this.afterResizeStart)
			this.afterResizeStart();
	}

	$el.onmousedown = function (ev) {
		if (! this.panning)
			return;
		this.panStartEvent(ev);
		return false;
	}
	$el.onmousewheel = function (ev) {
		if (! this.zooming)
			return;
		if (this.states.zoom.state)
			this.zoomStepEvent(ev);
		else this.zoomStartEvent(ev);
		return false;
	}


	function hammerInterAction () {
		$el.hammer = {
			Hammer:new Hammer($el, {prevent_default:true,inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput}),
			start:{scale:1,distance:{x:0,y:0}},
			keep:function (ev,mode) {
				if (mode === undefined || mode == 'zoom') {
					this.start.scale = ev.scale;
				}
				if (mode === undefined || mode == 'pan') {
					this.start.distance.x = ev.deltaX;
					this.start.distance.y = ev.deltaY;
				}
			},
			wasRunning:false,
			trigger:false
		}
		$el.hammer.Hammer.get('pinch').set({ enable: true });

		$el.hammer.Hammer.on('pinchstart', function(ev) {
			if ($el.beforePanStart)
				$el.beforePanStart();
			if ($el.beforeZoomStart)
				$el.beforeZoomStart();

			$el.states.pan.state = true;
			$el.states.zoom.state = true;

			if ($el.panStart)
				$el.panStart();
			if ($el.zoomStart)
				$el.zoomStart();

			$el.hammer.keep(ev);

			if (! $el.hammer.trigger) {
				$el.hammer.wasRunning = animating;
				if (! animating) $el.animate();
				$el.hammer.trigger = true;
			}

			if ($el.afterPanStart)
				$el.afterPanStart();
			if ($el.afterZoomStart)
				$el.afterZoomStart();

		});
		$el.hammer.Hammer.on('pinch', function(ev) {
			if ($el.beforePanStep)
				$el.beforePanStep();
			if ($el.beforeZoomStep)
				$el.beforeZoomStep();

			$el.panEvent(
				ev.deltaX-$el.hammer.start.distance.x,
				ev.deltaY-$el.hammer.start.distance.y);

			$el.zoomEvent(ev.center.x, ev.center.y,
				$el.hammer.start.scale-ev.scale);

			if ($el.panStep)
				$el.panStep();
			if ($el.zoomStep)
				$el.zoomStep();

			$el.hammer.keep(ev);

			if ($el.afterPanStep)
				$el.afterPanStep();
			if ($el.afterZoomStep)
				$el.afterZoomStep();
		});
		$el.hammer.Hammer.on('pinchend', function(ev) {
			if ($el.beforePanStop)
				$el.beforePanStop();
			if ($el.beforeZoomStop)
				$el.beforeZoomStop();

			$el.states.pan.state = true;
			$el.states.zoom.state = true;

			if ($el.panStop)
				$el.panStop();
			if ($el.zoomStop)
				$el.zoomStop();

			if (! $el.hammer.wasRunning) $el.stop();
			$el.hammer.trigger = false;

			if ($el.afterPanStop)
				$el.afterPanStop();
			if ($el.afterZoomStop)
				$el.afterZoomStop();
		});
	}



	// Basic Actions

	$el.fill = function (style) {
		if (style !== undefined)
			this.context.fillStyle = style;
		this.context.fill();
		this.context.dirty = true;
	}
	$el.stroke = function (style) {
		if (style !== undefined)
			this.context.strokeStyle = style;
		this.context.stroke();
		this.context.dirty = true;
	}

	$el.beginPath = function () {
		return this.context.beginPath.apply(this.context, arguments);
	}
	$el.closePath = function () {
		return this.context.closePath.apply(this.context, arguments);
	}
	$el.clip = function () {
		return this.context.clip.apply(this.context, arguments);
	}



	// Other Shapes

	this.shape = function (shape, conf) {
		this.beginPath();
		switch (shape) {
			case 'circle':
				this.circle(conf.x, conf.y, conf.r);
				break;
			case 'arc':
				this.arc(conf.x, conf.y, conf.r, conf.s, conf.e);
				break;
			case 'square':
				this.square(conf.x, conf.y, conf.r);
				break;
			case 'rect':
			case 'rectangle':
				this.rect(conf.x, conf.y, conf.w, conf.h);
				break;
			case 'triangle':
				this.triangle(conf.x, conf.y, conf.x1, conf.y1, conf.x2, conf.y2);
				break;
		}
	}
	this.fillShape = function (shape, conf, style) {
		this.shape(shape, conf);
		this.fill(style);
	}
	this.strokeShape = function (shape, conf, style) {
		this.shape(shape, conf);
		this.stroke(style);
	}
	this.paintShape = function (shape, conf, fillStyle, strokeStyle) {
		this.shape(shape, conf);
		this.fill(fillStyle);
		this.stroke(strokeStyle);

	}


	$el.triangle = function (x, y, x1, y1, x2, y2) {
		this.path([x,y], [x1,y1], [x2,y2]);
	}
	$el.fillTriangle = function (x, y, x1, y1, x2, y2, style) {
		this.triangle(x, y, x1, y1, x2, y2);
		this.fill(style);
	}
	$el.strokeTriangle = function (x, y, x1, y1, x2, y2, style) {
		this.triangle(x, y, x1, y1, x2, y2);
		this.stroke(style);
	}
	$el.paintTriangle = function (x, y, x1, y1, x2, y2, fillStyle, strokeStyle) {
		this.triangle(x, y, x1, y1, x2, y2);
		this.fill(fillStyle);
		this.stroke(strokeStyle);
	}




	// Rectangles

	$el.rect = function (x, y, w, h) {
		var rpos;
		if (! this.shapeOrigin)
			rpos = this.position({x:x, y:y});
		else {
			var off = {x:0,y:0};
			if (this.shapeOrigin.indexOf('top') != -1)
				off.y = 0;
			else if (this.shapeOrigin.indexOf('bottom') != -1)
				off.y = -h;
			else if (this.shapeOrigin.indexOf('center') != -1)
				off.y = -h/2;
			if (this.shapeOrigin.indexOf('left') != -1)
				off.x = 0;
			else if (this.shapeOrigin.indexOf('right') != -1)
				off.x = -w;
			else if (this.shapeOrigin.indexOf('center') != -1)
				off.x = -w/2;
			var i = 1;
			if (this.inverseY) i = -1;
			rpos = this.position({x:x+off.x, y:y+off.y*i});
		}
		this.context.rect(rpos.x, rpos.y, w, h);
	}
	$el.fillRect = function (x, y, w, h, style) {
		var rpos;
		if (! this.shapeOrigin)
			rpos = this.position({x:x, y:y});
		else {
			var off = {x:0,y:0};
			if (this.shapeOrigin.indexOf('top') != -1)
				off.y = 0;
			else if (this.shapeOrigin.indexOf('bottom') != -1)
				off.y = -h;
			else if (this.shapeOrigin.indexOf('center') != -1)
				off.y = -h/2;
			if (this.shapeOrigin.indexOf('left') != -1)
				off.x = 0;
			else if (this.shapeOrigin.indexOf('right') != -1)
				off.x = -w;
			else if (this.shapeOrigin.indexOf('center') != -1)
				off.x = -w/2;
			var i = 1;
			if (this.inverseY) i = -1;
			rpos = this.position({x:x+off.x, y:y+off.y*i});
		}
		if (style !== undefined)
			this.context.fillStyle = style;
		this.context.fillRect(rpos.x, rpos.y, w, h);
		this.context.dirty = true;
	}
	$el.strokeRect = function (x, y, w, h, style) {
		var rpos;
		if (! this.shapeOrigin)
			rpos = this.position({x:x, y:y});
		else {
			var off = {x:0,y:0};
			if (this.shapeOrigin.indexOf('top') != -1)
				off.y = 0;
			else if (this.shapeOrigin.indexOf('bottom') != -1)
				off.y = -h;
			else if (this.shapeOrigin.indexOf('center') != -1)
				off.y = -h/2;
			if (this.shapeOrigin.indexOf('left') != -1)
				off.x = 0;
			else if (this.shapeOrigin.indexOf('right') != -1)
				off.x = -w;
			else if (this.shapeOrigin.indexOf('center') != -1)
				off.x = -w/2;
			var i = 1;
			if (this.inverseY) i = -1;
			rpos = this.position({x:x+off.x, y:y+off.y*i});
		}
		if (style !== undefined)
			this.context.strokeStyle = style;
		this.context.strokeRect(rpos.x, rpos.y, w, h);
		this.context.dirty = true;
	}
	$el.clearRect = function (x, y, w, h) {
		var rpos;
		if (! this.shapeOrigin)
			rpos = this.position({x:x, y:y});
		else {
			var off = {x:0,y:0};
			if (this.shapeOrigin.indexOf('top') != -1)
				off.y = 0;
			else if (this.shapeOrigin.indexOf('bottom') != -1)
				off.y = -h;
			else if (this.shapeOrigin.indexOf('center') != -1)
				off.y = -h/2;
			if (this.shapeOrigin.indexOf('left') != -1)
				off.x = 0;
			else if (this.shapeOrigin.indexOf('right') != -1)
				off.x = -w;
			else if (this.shapeOrigin.indexOf('center') != -1)
				off.x = -w/2;
			var i = 1;
			if (this.inverseY) i = -1;
			rpos = this.position({x:x+off.x, y:y+off.y*i});
		}
		this.context.clearRect(rpos.x, rpos.y, w, h);
	}
	$el.paintRect = function (x, y, w, h, fillStyle, strokeStyle) {
		this.beginPath();
		this.rect(x, y, w, h);
		this.fill(fillStyle);
		this.stroke(strokeStyle);
	}

	$el.square = function (x, y, r) {
		this.rect(x, y, r, r);
	}
	$el.fillSquare = function (x, y, r, style) {
		this.fillRect(x, y, r, r, style);
	}
	$el.strokeSquare = function (x, y, r, style) {
		this.strokeRect(x, y, r, r, style);
	}
	$el.clearSquare = function (x, y, r) {
		this.clearRect(x, y, r, r);
	}
	$el.paintSquare = function (x, y, r, fillStyle, strokeStyle) {
		this.paintRect(x, y, r, r, fillStyle, strokeStyle);
	}


	// Paths

	$el.line = function () {
		this.path.apply(this, arguments);
	}
	$el.closedLine = function () {
		this.closedPath.apply(this, arguments);
	}
	$el.fillLine = function () {
		this.fillPath.apply(this, arguments);
	}
	$el.strokeLine = function () {
		this.strokePath.apply(this, arguments);
	}
	$el.paintLine = function () {
		this.paintPath.apply(this, arguments);
	}


	$el.path = function () {
		if (! arguments.length)
			return;
		if (arguments[0] instanceof Array && arguments[0][0] instanceof Array) {
			for (var a=0; a<arguments.length; a++)
				this.path.apply(this, arguments[a]);
		} else {
			this.beginPath();
			this.moveTo(arguments[0][0],arguments[0][1]);
			for (var a=1; a<arguments.length; a++)
				this.curveTo.apply(this, arguments[a]);

		}
	}
	$el.closedPath = function () {
		this.path.apply(this, arguments);
		this.closePath();
	}
	$el.fillPath = function (path, style) {
		this.closedPath.apply(this, path);
		this.fill(style);
	}
	$el.strokePath = function (path, style) {
		this.path.apply(this, path);
		this.stroke(style);
	}
	$el.paintPath = function (path, fillStyle, strokeStyle) {
		this.closedPath.apply(this, path);
		this.fill(fillStyle);
		this.stroke(strokeStyle);
	}

	$el.moveTo = function (x, y) {
		var rpos = this.position({x:x, y:y});
		return this.context.moveTo(rpos.x,rpos.y);
	}
	$el.lineTo = function (x, y) {
		var rpos = this.position({x:x, y:y});
		return this.context.lineTo(rpos.x,rpos.y);
	}
	$el.curveTo = function (args) {
		switch (arguments.length) {
			case 2:
				this.lineTo.apply(this, arguments);
				break;
			case 4:
				this.quadraticCurveTo.call(this, arguments[2], arguments[3], arguments[0], arguments[1]);
				break;
			case 5:
				this.arcTo.apply(this, arguments);
				break;
			case 6:
				this.bezierCurveTo.call(this, arguments[2], arguments[3], arguments[4], arguments[5], arguments[0], arguments[1]);
				break;
			default:
				if (args instanceof Array)
					this.curveTo.apply(this, args);
				else this.curveTo(arguments[0], arguments[1]);
				// else if (args instanceof Object) {
				// What then??
				// }
				break;
		}
	}

	$el.quadraticCurveTo = function (cpx, cpy, x, y) {
		var rpos = this.position({x:x, y:y}),
			rposcp = this.position({x:cpx, y:cpy});
		this.context.quadraticCurveTo(rposcp.x, rposcp.y, rpos.x, rpos.y);
	}
	$el.bezierCurveTo = function (cp1x, cp1y, cp2x, cp2y, x, y) {
		var rpos = this.position({x:x, y:y}),
			rposcp1 = this.position({x:cp1x, y:cp1y}),
			rposcp2 = this.position({x:cp2x, y:cp2y});
		this.context.bezierCurveTo(rposcp1.x, rposcp1.y, rposcp2.x, rposcp2.y, rpos.x, rpos.y);
	}
	$el.arcTo = function (x1, y1, x2, y2, r) {
		var rpos1 = this.position({x:x1, y:y1}),
			rpos2 = this.position({x:x2, y:y2});
		this.context.arcTo(rpos1.x, rpos1.y, rpos2.x, rpos2.y, r);
	}



	// Arcs

	$el.arc = function (x, y, r, a, b, anticlockwise) {
		var rpos;
		if (this.zoom < this.minShapeZoom)
			r *= this.minShapeZoom/this.zoom;
		if (! this.shapeOrigin)
			rpos = this.position({x:x, y:y});
		else {
			var off = {x:0,y:0};
			if (this.shapeOrigin.indexOf('top') != -1)
				off.y = r;
			else if (this.shapeOrigin.indexOf('bottom') != -1)
				off.y = -r;
			if (this.shapeOrigin.indexOf('left') != -1)
				off.x = r;
			else if (this.shapeOrigin.indexOf('right') != -1)
				off.x = -r;
			var i = 1;
			if (this.inverseY) i = -1;
			rpos = this.position({x:x+off.x, y:y+off.y*i});
		}
		return this.context.arc(rpos.x, rpos.y, r, a, b, anticlockwise);
	}
	$el.fillArc = function (x, y, r, a, b, style, anticlockwise) {
		this.beginPath();
		this.arc(x, y, r, a, b, anticlockwise);
		this.closePath();
		this.fill(style);
	}
	$el.strokeArc = function (x, y, r, a, b, style, anticlockwise) {
		this.beginPath();
		this.arc(x, y, r, a, b, anticlockwise);
		this.stroke(style);
	}
	$el.paintArc = function (x, y, r, a, b, fillStyle, strokeStyle, anticlockwise) {
		this.beginPath();
		this.arc(x, y, r, a, b, anticlockwise);
		this.fill(fillStyle);
		this.stroke(strokeStyle);
	}


	$el.circle = function (x, y, r) {
		return this.arc(x, y, r, 0, Math.PI*2);
	}
	$el.fillCircle = function (x, y, r, style) {
		return this.fillArc(x, y, r, 0, Math.PI*2, style);
	}
	$el.strokeCircle = function (x, y, r, style) {
		return this.strokeArc(x, y, r, 0, Math.PI*2, style);
	}
	$el.paintCircle = function (x, y, r, fillStyle, strokeStyle) {
		return this.paintArc(x, y, r, 0, Math.PI*2, fillStyle, strokeStyle);
	}

	$el.ellipse = function (x, y, radiusX, radiusY, rotation, startAngle, endAngle, anticlockwise) {
		var rpos;
		if (this.zoom < this.minShapeZoom) {
			radiusX *= this.minShapeZoom/this.zoom;
			radiusY *= this.minShapeZoom/this.zoom;
		}
		if (! this.shapeOrigin)
			rpos = this.position({x:x, y:y});
		else {
			var off = {x:0,y:0};
			if (this.shapeOrigin.indexOf('top') != -1)
				off.y = radiusY;
			else if (this.shapeOrigin.indexOf('bottom') != -1)
				off.y = -radiusY;
			if (this.shapeOrigin.indexOf('left') != -1)
				off.x = radiusX;
			else if (this.shapeOrigin.indexOf('right') != -1)
				off.x = -radiusX;
			var i = 1;
			if (this.inverseY) i = -1;
			rpos = this.position({x:x+off.x, y:y+off.y*i});
		}
		return this.context.ellipse(rpos.x, rpos.y, radiusX, radiusY, rotation, startAngle, endAngle, anticlockwise);
	}



	// Text

	$el.fillText = function (text, x, y, style, maxWidth) {
		var rpos;
		if (! this.shapeOrigin)
			rpos = this.position({x:x, y:y});
		else {
			var off = {x:0,y:0};

			if (this.shapeOrigin.indexOf('top') != -1) {
				var h = parseInt(this.context.font.split(' ')[0]);
				off.y = h;
			} else if (this.shapeOrigin.indexOf('bottom') != -1) {
				off.y = 0;
			} else if (this.shapeOrigin.indexOf('center') != -1) {
				var h = parseInt(this.context.font.split(' ')[0]);
				off.y = h/2;
			}
			if (this.shapeOrigin.indexOf('left') != -1) {
				off.x = 0;
			} else if (this.shapeOrigin.indexOf('right') != -1) {
				var w = this.measureText(text).width;
				if (maxWidth !== undefined && w > maxWidth)
					w = maxWidth;
				off.x = -w;
			} else if (this.shapeOrigin.indexOf('center') != -1) {
				var w = this.measureText(text).width;
				if (maxWidth !== undefined && w > maxWidth)
					w = maxWidth;
				off.x = -w/2;
			}
			var i = 1;
			if (this.inverseY) i = -1;
			rpos = this.position({x:x+off.x, y:y+off.y*i});
		}
		if (style !== undefined)
			this.context.fillStyle = style;
		// if (maxWidth !== undefined)
		// 	maxWidth *= this.zoom;
		// this.context.save();
		// this.context.scale(this.zoom, this.zoom);
		this.context.fillText(text, rpos.x, rpos.y, maxWidth);
		// this.context.restore();
		this.context.dirty = true;
	}
	$el.strokeText = function (text, x, y, style, maxWidth) {
		var rpos;
		if (! this.shapeOrigin)
			rpos = this.position({x:x, y:y});
		else {
			var off = {x:0,y:0};

			if (this.shapeOrigin.indexOf('top') != -1) {
				var h = parseInt(this.context.font.split(' ')[0]);
				off.y = h;
			} else if (this.shapeOrigin.indexOf('bottom') != -1) {
				off.y = 0;
			} else if (this.shapeOrigin.indexOf('center') != -1) {
				var h = parseInt(this.context.font.split(' ')[0]);
				off.y = h/2;
			}
			if (this.shapeOrigin.indexOf('left') != -1) {
				off.x = 0;
			} else if (this.shapeOrigin.indexOf('right') != -1) {
				var w = this.measureText(text).width;
				if (maxWidth !== undefined && w > maxWidth)
					w = maxWidth;
				off.x = -w;
			} else if (this.shapeOrigin.indexOf('center') != -1) {
				var w = this.measureText(text).width;
				if (maxWidth !== undefined && w > maxWidth)
					w = maxWidth;
				off.x = -w/2;
			}
			var i = 1;
			if (this.inverseY) i = -1;
			rpos = this.position({x:x+off.x, y:y+off.y*i});
		}
		if (style !== undefined)
			this.context.strokeStyle = style;
		// if (maxWidth !== undefined)
		// 	maxWidth *= this.zoom;
		// this.context.font = getFont();
		this.context.strokeText(text, rpos.x, rpos.y, maxWidth);
		this.context.dirty = true;
	}
	$el.paintText = function (text, x, y, fillStyle, strokeStyle, maxWidth) {
		this.fillText(text, x, y, fillStyle, maxWidth);
		this.strokeText(text, x, y, strokeStyle, maxWidth);
	}
	$el.measureText = function () {
		return this.context.measureText.apply(this.context, arguments);
	}


	// Transformations

	$el.scale = function () {
		return this.context.scale.apply(this.context, arguments);
	}
	$el.rotate = function () {
		return this.context.rotate.apply(this.context, arguments);
	}
	$el.translate = function () {
		return this.context.translate.apply(this.context, arguments);
	}
	$el.transform = function () {
		return this.context.transform.apply(this.context, arguments);
	}
	$el.setTransform = function () {
		return this.context.setTransform.apply(this.context, arguments);
	}


	// Gradients

	$el.createLinearGradient = function (x, y, x1, y1) {
		var rpos = this.position({x:x, y:y}),
			rpos1 = this.position({x:x1, y:y1});
		return this.context.createLinearGradient(rpos.x, rpos.y, rpos1.x, rpos1.y);
	}
	$el.createPattern = function () {
		return this.context.createPattern.apply(this.context, arguments);
	}
	$el.createRadialGradient = function (x, y, r, x1, y1, r1) {
		var rpos = this.position({x:x, y:y}),
			rpos1 = this.position({x:x1, y:y1});
		return this.context.createRadialGradient(rpos.x, rpos.y, r, rpos1.x, rpos1.y, r1);
	}


	// Image

	$el.createImageData = function () {
		return this.context.createImageData.apply(this.context, arguments);
	}
	$el.getImageData = function () {
		return this.context.getImageData.apply(this.context, arguments);
	}
	$el.putImageData = function () {
		return this.context.putImageData.apply(this.context, arguments);
	}


	// State

	$el.save = function () {
		return this.context.save.apply(this.context, arguments);
	}
	$el.restore = function () {
		return this.context.restore.apply(this.context, arguments);
	}


	// Misc
	$el.isPointInPath = function () {
		return this.context.isPointInPath.apply(this.context, arguments);
	}


	// Save the first for last
	init();
}






// var canvs = document.getElementsByTagName('canvas');
// for (var c=0; c<canvs.length; c++) {
// 	var $el = canvs[c],
// 		canv = Canvas($el);
// }
